QUEST_ELDER			= 1
QUEST_TORIN			= 2
QUEST_OZORIK		= 3
QUEST_YOHJI_BAT		= 4
QUEST_YOHJI_RAT		= 5
QUEST_ANCIENT_PART	= 6


function CreateAllQuests()

	Quest(QUEST_ELDER, Q_UNKNOWN, 
		"The Village Elder asked you to kill the demon who attacks villagers and has occupied the caves to the west of the village.", 
		"", 
		"")

	Quest(QUEST_TORIN, Q_UNKNOWN, 
		"Torin, the Dwarven King asked you to switch on gas pump at the bottom of gold mine.", 
		"", 
		"")

	Quest(QUEST_OZORIK, Q_UNKNOWN, 
		"Ozorick, the royal guard captain, have problems with an orc-party.", 
		"", 
		"")

	Quest(QUEST_YOHJI_BAT, Q_UNKNOWN, 
		"Yohjishiro, the elven wizard asked you bring a bat wing.", 
		"", 
		"")

	Quest(QUEST_YOHJI_RAT, Q_UNKNOWN, 
		"Yohjishiro, the elven wizard asked you bring a rat tail.", 
		"", 
		"")

	Quest(QUEST_ANCIENT_PART, Q_UNKNOWN, 
		"Ahk-Ulan asked you to bring 3 parts of ancient machine.", 
		"", 
		"")

end