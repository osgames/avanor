/*
This file is part of "Avanor, the Land of Mystery" roguelike game
Home page: http://www.avanor.com/
Copyright (C) 2000-2003 Vadim Gaidukevich

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __INCL_I_H
#define __INCL_I_H

#include "itemdef.h"
#include "xcap.h"
#include "xarmor.h"
#include "xcloak.h"
#include "xboots.h"
#include "xgloves.h"
#include "xring.h"
#include "xamulet.h"
#include "xweapon.h"
#include "xmissileweapon.h"
#include "xmissile.h"
#include "xcorpse.h"
#include "xmoney.h"
#include "xshield.h"
#include "xbook.h"
#include "xpotion.h"
#include "xscroll.h"
#include "xration.h"

#endif
